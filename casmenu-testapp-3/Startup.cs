﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(casmenu_testapp_3.Startup))]
namespace casmenu_testapp_3
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
