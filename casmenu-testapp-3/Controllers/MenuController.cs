﻿using System.Net.Http;
using System.Web.Http;
using casmenu_testapp_3.Controllers.Base;

namespace casmenu_testapp_3.Controllers
{
    [Authorize]
    public class MenuController : BaseController
    {

        //GET: /api/menu/get/
        [HttpGet]
        public HttpResponseMessage Get(int week)
        {
            try
            {
                var menu = DataService.GetMenu(week);
                return ToJson(menu);
            }
            catch (HttpResponseException e)
            {
                return e.Response;
            }
        }

        //[HttpPost]
        //public HttpResponseMessage Post(int week, [FromBody]Menu menu)
        //{
        //    try
        //    {
        //        var updatedMenu = DataService.UpdateMenu(week, menu);
        //        return ToJson(updatedMenu);
        //    }
        //    catch (HttpResponseException e)
        //    {
        //        return e.Response;
        //    }
        //}

    }
}
