﻿using System;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using casmenu_testapp_3.Controllers.Base;
using casmenu_testapp_3.Exceptions;
using casmenu_testapp_3.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace casmenu_testapp_3.Controllers
{
    [Authorize]
    public class OrderController : BaseController
    {

        //GET: /api/order/get/
        [HttpGet]
        public HttpResponseMessage Get(int week)
        {
            var currentUser = HttpContext.Current.GetOwinContext()
                .GetUserManager<ApplicationUserManager>()
                .FindByName(User.Identity.Name);

            try
            {
                var order = DataService.GetOrder(week, email: currentUser.Email, name: currentUser.UserName);
                return ToJson(order);
            }
            catch (HttpResponseException e)
            {
                return e.Response;
            }
        }

        //POST: /api/order/post
        [HttpPost]
        public HttpResponseMessage Post(int week, [FromBody]Order order)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            try
            {
                var updatedOrder = DataService.UpdateOrder(week, order);
                return ToJson(updatedOrder);
            }
            catch (HttpResponseException e)
            {
                return e.Response;
            }
        }

    }
}
