﻿using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using casmenu_testapp_3.Exceptions;
using casmenu_testapp_3.Services;
using casmenu_testapp_3.Services.Google;
using Google.Apis.Sheets.v4;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace casmenu_testapp_3.Controllers.Base
{
    [CasMenuExceptionFilter]
    public class BaseController : ApiController
    {
        private static readonly string[] Scopes = { SheetsService.Scope.Spreadsheets };
        private static readonly string ClientSecretPath = System.Web.HttpContext.Current.Server
            .MapPath("~/client_secret.json");
        private const string ApplicationName = "CasMenu";
        private const string SpreadsheetId = "10PiN6m8iu2D089pXsz-e7IvAKGO5OJXLevMcGc4uPHk";

        protected readonly SheetsDataService DataService;
        protected readonly SheetsDataRepository DataRepository;
        protected readonly GoogleService GoogleService;
        protected readonly SheetsApiService SheetsApiService;

        public BaseController()
        {
            GoogleService = new GoogleService(ClientSecretPath, ApplicationName, Scopes);
            SheetsApiService = new SheetsApiService(SpreadsheetId, GoogleService);
            DataRepository = new SheetsDataRepository(SheetsApiService);
            DataService = new SheetsDataService(DataRepository);
        }

        protected HttpResponseMessage ToJson(dynamic obj)
        {
            var response = Request.CreateResponse(HttpStatusCode.OK);
            response.Content = new StringContent(
                JsonConvert.SerializeObject(
                    obj, Formatting.Indented, new JsonSerializerSettings
                    {
                        ContractResolver = new CamelCasePropertyNamesContractResolver()
                    }),
                Encoding.UTF8,
                "application/json");

            return response;
        }

    }
}
