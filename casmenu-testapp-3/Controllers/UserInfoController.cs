﻿using System.Net.Http;
using System.Web;
using System.Web.Http;
using casmenu_testapp_3.Controllers.Base;
using casmenu_testapp_3.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace casmenu_testapp_3.Controllers
{
    public class UserInfoController : BaseController
    {
        [HttpGet]
        public HttpResponseMessage CurrentUser()
        {
            var currentUser = HttpContext.Current.GetOwinContext()
                .GetUserManager<ApplicationUserManager>()
                .FindByName(User.Identity.Name);

            var user = new User(currentUser.UserName, currentUser.Email);
            return ToJson(user);
        }


    }
}
