﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Google.Apis.Sheets.v4.Data;

namespace casmenu_testapp_3.Extensions
{
    public static class SheetsExtensionMethods
    {
        public static List<List<string>> RowDataAsString(this GridData data)
        {
            var result = new List<List<string>>();
            if (data == null) return result;

            if (data.RowData.Any())
            {
                result = data.RowData
                    .Select(row => row.Values.Select(cell => cell.FormattedValue).ToList())
                    .ToList();
            }

            return result;
        }
    }
}