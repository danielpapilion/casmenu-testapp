﻿using System.Collections.Generic;
using System.Linq;
using casmenu_testapp_3.Exceptions;
using casmenu_testapp_3.Models;
using casmenu_testapp_3.Models.Base;
using casmenu_testapp_3.Services;

namespace casmenu_testapp_3.Extensions
{
    public static class ListExtensionMethods
    {
        public static List<string> FindData<T>(this List<List<string>> data, params string[] criteria) where T : CasMenuModel
        {
            if (!data.IsValidCasMenuData()) throw new InvalidCasMenuDataException("Cannot get UpdateOrder data");

            if (typeof(T) == typeof(Menu))
            {
                var menuData = data[SheetConstants.MenuRowIndex];
                return FormatToMenuData(menuData);
            }

            if (typeof(T) == typeof(Order))
            {
                data.RemoveRange(0, 4);
                return data.Filter(criteria).FirstOrDefault();
            }

            return data.FirstOrDefault();
        }

        public static bool IsValidCasMenuData(this List<List<string>> data)
        {
            if (data.Count < SheetConstants.MenuRowIndex + 1) return false;

            if (data[SheetConstants.MenuRowIndex].Count != SheetConstants.MenuColumnsCount)
                return false;

            if (data.Count < SheetConstants.OrdersStartRowIndex + 1) return true;

            for (var i = SheetConstants.OrdersStartRowIndex; i < data.Count; i++)
            {
                if (data[i].Count != SheetConstants.OrdersColumnsCountWithEmail && data[i].Count != SheetConstants.OrdersColumnsCountWithoutEmail)
                    return false;
            }

            return true;
        }

        public static List<List<string>> Filter(this List<List<string>> data, params string[] criteria)
        {
            var result = new List<List<string>>();
            foreach (var str in criteria)
            {
                foreach (var row in data)
                {
                    if (row.Contains(str) && !result.Contains(row))
                        result.Add(row);
                }
            }

            return result;
        }

        public static List<string> FormatToMenuData(this List<string> data)
        {
            const int menuDataColumnsCount = 20;

            var length = data.Count;
            data.RemoveAt(0);

            if (length == SheetConstants.MenuColumnsCount)
                data.RemoveRange(menuDataColumnsCount, 2);

            return data;
        }
    }
}