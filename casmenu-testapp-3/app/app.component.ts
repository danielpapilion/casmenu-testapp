﻿import { Component, OnInit } from "@angular/core"
import Casmenuservice = require("./services/casmenu.service");
import CasMenuService = Casmenuservice.CasMenuService;
import Order = require("./models/order");
import User = Order.User;
import Global = require("./shared/global");
import Global1 = Global.Global;
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

@Component({
    selector: "casmenu-app",
    templateUrl: "app/app.component.html",
    styleUrls: [ "app/style.css"]
})

export class AppComponent implements OnInit {
    user: User;
    indLoading = false;
    msg: string;
    active: string;

    constructor(router: Router, title: Title, private menuService: CasMenuService) {
        router.events.subscribe((event) => {
            title.setTitle(`CasMenu - ${this.getTitleFor(router.url)}`);
            this.active = router.url;
        });
    }

    ngOnInit(): void { this.loadUser(); }

    loadUser(): void {
        this.indLoading = true;
        this.menuService.get(Global1.BASE_USER_ENDPOINT)
            .subscribe(result => {
                    this.user = new User(result.name, result.email);
                },
                error => this.msg = error
        );
        this.indLoading = false;
    }

    getTitleFor(url: string) {
        if (url == "/menu") {
            return "Menü";
        } 
        if (url == "/ordermenu") {
            return "Rendelés";
        }
    }


    //TODO
    //https://stackoverflow.com/questions/34602806/how-to-change-page-title-in-angular2-router
}