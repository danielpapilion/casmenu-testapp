﻿import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule, Router } from '@angular/router';
import { Title } from '@angular/platform-browser';
import { MenuComponent } from './components/menu.component';
import { OrderMenuComponent } from './components/ordermenu.component';

const appRoutes: Routes = [
    { path: '', redirectTo: 'ordermenu', pathMatch: 'full' },
    { path: 'menu', component: MenuComponent },
    { path: 'ordermenu', component: OrderMenuComponent }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes);