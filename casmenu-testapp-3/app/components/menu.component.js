"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var casmenu_service_1 = require("../services/casmenu.service");
var forms_1 = require("@angular/forms");
var menu_1 = require("../models/menu");
var order_1 = require("../models/order");
var global_1 = require("../Shared/global");
var MenuComponent = (function () {
    function MenuComponent(fb, menuService) {
        this.fb = fb;
        this.menuService = menuService;
        this.days = {
            0: "Hétfő",
            1: "Kedd",
            2: "Szerda",
            3: "Csütörtök",
            4: "Péntek"
        };
        this.indLoading = false;
        this.week = 28;
        this.menuItems = new Array();
        this.orderItems = new Array();
    }
    MenuComponent.prototype.ngOnInit = function () {
        this.loadOrder();
        this.loadMenu();
    };
    MenuComponent.prototype.loadMenu = function () {
        var _this = this;
        this.indLoading = true;
        this.menuService.get(global_1.Global.BASE_MENU_ENDPOINT, this.week)
            .subscribe(function (result) {
            _this.menuItems = menu_1.Item.fromJson(result);
            _this.indLoading = false;
        }, function (error) { return _this.msg = error; });
    };
    MenuComponent.prototype.loadOrder = function () {
        var _this = this;
        this.indLoading = true;
        this.menuService.get(global_1.Global.BASE_ORDER_ENDPOINT, this.week)
            .subscribe(function (result) {
            _this.orderItems = order_1.OrderItemViewModel.fromJson(result);
            _this.user = order_1.User.fromJson(result);
            _this.indLoading = false;
        }, function (error) { return _this.msg = error; });
    };
    return MenuComponent;
}());
MenuComponent = __decorate([
    core_1.Component({
        templateUrl: "app/components/menu.component.html",
        styleUrls: ["app/style.css"],
    }),
    __metadata("design:paramtypes", [forms_1.FormBuilder, casmenu_service_1.CasMenuService])
], MenuComponent);
exports.MenuComponent = MenuComponent;
//# sourceMappingURL=menu.component.js.map