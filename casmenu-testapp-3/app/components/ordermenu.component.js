"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var casmenu_service_1 = require("../services/casmenu.service");
var menu_1 = require("../models/menu");
var order_1 = require("../models/order");
var global_1 = require("../Shared/global");
var OrderMenuComponent = (function () {
    function OrderMenuComponent(menuService) {
        this.menuService = menuService;
        this.week = 29;
        this.menuPrice = 400;
        this.costChangeType = 1;
        this.days = {
            0: "Hétfő",
            1: "Kedd",
            2: "Szerda",
            3: "Csütörtök",
            4: "Péntek"
        };
        this.indLoading = false;
        this.cost = 0;
        this.menuItems = new Array();
        this.orderItems = new Array();
        this.costChange = new core_1.EventEmitter();
        this.updateEvent = new core_1.EventEmitter();
    }
    OrderMenuComponent.prototype.ngOnInit = function () {
        this.loadOrder();
        this.loadMenu();
    };
    OrderMenuComponent.prototype.loadMenu = function () {
        var _this = this;
        this.indLoading = true;
        this.menuService.get(global_1.Global.BASE_MENU_ENDPOINT, this.week)
            .subscribe(function (result) {
            _this.menuItems = menu_1.Item.fromJson(result);
            _this.indLoading = false;
        }, function (error) { return _this.msg = error; });
    };
    OrderMenuComponent.prototype.loadOrder = function () {
        var _this = this;
        this.indLoading = true;
        this.menuService.get(global_1.Global.BASE_ORDER_ENDPOINT, this.week)
            .subscribe(function (result) {
            _this.orderItems = order_1.OrderItemViewModel.fromJson(result);
            _this.user = order_1.User.fromJson(result);
            _this.payed = result.payed;
            _this.prise = result.prise;
            for (var _i = 0, _a = _this.orderItems; _i < _a.length; _i++) {
                var orderItem = _a[_i];
                _this.cost += orderItem.isNotEmpty ? 400 : 0;
            }
            _this.indLoading = false;
        }, function (error) { return _this.msg = error; });
    };
    OrderMenuComponent.prototype.sendOrder = function () {
        var _this = this;
        this.indLoading = true;
        var orderItems = new Array();
        for (var _i = 0, _a = this.orderItems; _i < _a.length; _i++) {
            var item = _a[_i];
            orderItems.push(order_1.OrderItemViewModel.toOrderItem(item));
        }
        var model = new order_1.Order(this.user, orderItems, this.prise, this.payed);
        this.menuService.post(global_1.Global.BASE_ORDER_ENDPOINT, this.week, model)
            .subscribe(function (result) {
            _this.isUpdatedSuccessfully =
                _this.user.name === result.user.name &&
                    _this.user.email === result.user.email &&
                    order_1.OrderItemViewModel.equals(_this.orderItems, order_1.OrderItemViewModel.fromJson(result));
            _this.updateEvent.emit(_this.isUpdatedSuccessfully);
        });
        this.indLoading = false;
    };
    OrderMenuComponent.prototype.onSelectSoup = function (index, soup) {
        if (this.orderItems[index].isNotEmpty) {
            this.orderItems[index].soup = this.menuItems[index].soups.indexOf(soup);
        }
    };
    OrderMenuComponent.prototype.onSelectMeat = function (index, meat) {
        if (this.orderItems[index].isNotEmpty) {
            this.orderItems[index].meat = this.menuItems[index].meats.indexOf(meat);
        }
    };
    OrderMenuComponent.prototype.onIsNotEmptyChange = function (index) {
        this.changeCost(index);
        if (!this.orderItems[index].isNotEmpty) {
            this.orderItems[index].soup = 0;
            this.orderItems[index].meat = 0;
        }
    };
    OrderMenuComponent.prototype.changeCost = function (index) {
        this.costChangeType = this.orderItems[index].isNotEmpty ? 1 : -1;
        this.cost += this.costChangeType * this.menuPrice;
        this.costChange.emit(this.cost);
    };
    return OrderMenuComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], OrderMenuComponent.prototype, "cost", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Boolean)
], OrderMenuComponent.prototype, "isUpdatedSuccessfully", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", core_1.EventEmitter)
], OrderMenuComponent.prototype, "costChange", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", core_1.EventEmitter)
], OrderMenuComponent.prototype, "updateEvent", void 0);
OrderMenuComponent = __decorate([
    core_1.Component({
        templateUrl: "app/components/ordermenu.component.html",
        styleUrls: ["app/style.css"],
    }),
    __metadata("design:paramtypes", [casmenu_service_1.CasMenuService])
], OrderMenuComponent);
exports.OrderMenuComponent = OrderMenuComponent;
//# sourceMappingURL=ordermenu.component.js.map