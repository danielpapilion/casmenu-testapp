﻿import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CasMenuService } from '../services/casmenu.service';
import { Item, Soup, Meat } from '../models/menu';
import { OrderItemViewModel, OrderItem, User, Order } from '../models/order';
import { DBOperation } from '../Shared/enum';
import { Global } from '../Shared/global';
import { IDictionary } from '../Shared/IDictionary';


@Component({
    templateUrl: "app/components/ordermenu.component.html",
    styleUrls: ["app/style.css"],
})

export class OrderMenuComponent implements OnInit {
    week = 29;
    user: User;
    menuItems: Item[];
    orderItems: OrderItemViewModel[];
    payed: boolean;
    prise: number;
    menuPrice = 400;
    costChangeType = 1;
    days: IDictionary<string> = {
        0: "Hétfő",
        1: "Kedd",
        2: "Szerda",
        3: "Csütörtök",
        4: "Péntek"
    };

    msg: string;
    indLoading = false;
    dbops: DBOperation;

    constructor(private menuService: CasMenuService) {
        this.menuItems = new Array<Item>();
        this.orderItems = new Array<OrderItemViewModel>();
        this.costChange = new EventEmitter();
        this.updateEvent = new EventEmitter();
    }

    ngOnInit(): void {
        this.loadOrder();
        this.loadMenu();
    }

    loadMenu(): void {
        this.indLoading = true;
        this.menuService.get(Global.BASE_MENU_ENDPOINT, this.week)
            .subscribe(result =>
            {
                    this.menuItems = Item.fromJson(result);
                    this.indLoading = false;
            },
                error => this.msg = error
            );
    }

    loadOrder(): void {
        this.indLoading = true;
        this.menuService.get(Global.BASE_ORDER_ENDPOINT, this.week)
            .subscribe(result => {
                    this.orderItems = OrderItemViewModel.fromJson(result);
                    this.user = User.fromJson(result);
                    this.payed = result.payed;
                    this.prise = result.prise;
                    for (var orderItem of this.orderItems) {
                        this.cost += orderItem.isNotEmpty ? 400 : 0;
                    }
                    this.indLoading = false;
                },
                error => this.msg = error
            );
    }

    sendOrder() {
        this.indLoading = true;
        var orderItems = new Array<OrderItem>();
        for (var item of this.orderItems) {
            orderItems.push(OrderItemViewModel.toOrderItem(item));
        }
        var model = new Order(this.user, orderItems, this.prise, this.payed);
        this.menuService.post(Global.BASE_ORDER_ENDPOINT, this.week, model)
            .subscribe(result => {
                this.isUpdatedSuccessfully =
                    this.user.name === result.user.name &&
                    this.user.email === result.user.email &&
                    OrderItemViewModel.equals(this.orderItems, OrderItemViewModel.fromJson(result));
                this.updateEvent.emit(this.isUpdatedSuccessfully);
            });
        this.indLoading = false;
    }



    onSelectSoup(index: number, soup: Soup) {
        if (this.orderItems[index].isNotEmpty) {

            this.orderItems[index].soup = this.menuItems[index].soups.indexOf(soup);
        }
    }

    onSelectMeat(index: number, meat: Meat) {
        if (this.orderItems[index].isNotEmpty) {
            this.orderItems[index].meat = this.menuItems[index].meats.indexOf(meat);
        }
    }

    onIsNotEmptyChange(index: number) {
        this.changeCost(index);

        if (!this.orderItems[index].isNotEmpty) {
            this.orderItems[index].soup = 0;
            this.orderItems[index].meat = 0;
        }
    }

    @Input() cost = 0;
    @Input() isUpdatedSuccessfully: boolean;
    @Output() costChange: EventEmitter<number>;
    @Output() updateEvent: EventEmitter<boolean>;

    changeCost(index: number) {
        this.costChangeType = this.orderItems[index].isNotEmpty ? 1 : -1;
        this.cost += this.costChangeType * this.menuPrice;
        this.costChange.emit(this.cost);
    }    
}

