﻿import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { CasMenuService } from '../services/casmenu.service';
import { FormBuilder } from '@angular/forms';
import { Item, Soup, Meat } from '../models/menu';
import { OrderItemViewModel, OrderItem, User, Order } from '../models/order';
import { DBOperation } from '../Shared/enum';
import { Global } from '../Shared/global';
import { IDictionary } from '../Shared/IDictionary';


@Component({
    templateUrl: "app/components/menu.component.html",
    styleUrls: ["app/style.css"],
})
export class MenuComponent implements OnInit {
    week: number;
    user: User;
    menuItems: Item[];
    orderItems: OrderItemViewModel[];
    days: IDictionary<string> = {
        0: "Hétfő",
        1: "Kedd",
        2: "Szerda",
        3: "Csütörtök",
        4: "Péntek"
    };

    msg: string;
    indLoading = false;
    dbops: DBOperation;

    constructor(private fb: FormBuilder, private menuService: CasMenuService) {
        this.week = 28;
        this.menuItems = new Array<Item>();
        this.orderItems = new Array<OrderItemViewModel>();

    }

    ngOnInit(): void {
        this.loadOrder();
        this.loadMenu();
    }

    loadMenu(): void {
        this.indLoading = true;
        this.menuService.get(Global.BASE_MENU_ENDPOINT, this.week)
            .subscribe(result => {
                    this.menuItems = Item.fromJson(result);
                    this.indLoading = false;
                },
                error => this.msg = error
            );
    }

    loadOrder(): void {
        this.indLoading = true;
        this.menuService.get(Global.BASE_ORDER_ENDPOINT, this.week)
            .subscribe(result => {
                    this.orderItems = OrderItemViewModel.fromJson(result);
                    this.user = User.fromJson(result);
                    this.indLoading = false;
                },
                error => this.msg = error
            );
    }
}