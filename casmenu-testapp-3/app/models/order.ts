﻿export class OrderItemViewModel {
    constructor(
        public isNotEmpty: boolean,
        public soup: number,
        public meat: number) { }

    public static fromJson(json: any): OrderItemViewModel[] {
        var orderItems = new Array<OrderItemViewModel>();
        for (var orderItem of json.orderItems) {
            orderItems.push(new OrderItemViewModel(!orderItem.isEmpty, orderItem.soup, orderItem.meat));
        }

        return orderItems;
    }

    public static toOrderItem(viewModel: OrderItemViewModel): OrderItem {
        return new OrderItem(!viewModel.isNotEmpty, viewModel.soup, viewModel.meat);
    } 

    public static equals(one: OrderItemViewModel[], other: OrderItemViewModel[]): boolean {
        if (one.length != other.length)
            return false;
        else {
            for (var i = 0; i < one.length; i++) {
                if (one[i].isNotEmpty !== other[i].isNotEmpty ||
                    one[i].soup !== other[i].soup || one[i].meat !== other[i].meat)
                    return false;
            }
        }

        return true;
    }

}

export class OrderItem {
    constructor(
        public isEmpty: boolean,
        public soup: number,
        public meat: number) { }
}

export class User {
    constructor(
        public name: string,
        public email: string) { }

    public static fromJson(json: any): User {
        return new User(json.user.name, json.user.email);
    }
}

export class Order {
    constructor(
        public user: User,
        public orderItems: Array<OrderItem>,
        public prise: number,
        public payed: boolean) { }

}