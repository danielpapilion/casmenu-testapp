"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Soup = (function () {
    function Soup(name) {
        this.name = name;
    }
    return Soup;
}());
exports.Soup = Soup;
var Meat = (function () {
    function Meat(name) {
        this.name = name;
    }
    return Meat;
}());
exports.Meat = Meat;
var Item = (function () {
    function Item(soups, meats) {
        this.soups = soups;
        this.meats = meats;
    }
    Item.fromJson = function (json) {
        var itemsArray = new Array();
        for (var _i = 0, _a = json.items; _i < _a.length; _i++) {
            var item = _a[_i];
            var soupsArray = new Array();
            for (var _b = 0, _c = item.soups; _b < _c.length; _b++) {
                var soup = _c[_b];
                soupsArray.push(new Soup(soup.name));
            }
            var meatsArray = new Array();
            for (var _d = 0, _e = item.meats; _d < _e.length; _d++) {
                var meat = _e[_d];
                meatsArray.push(new Meat(meat.name));
            }
            itemsArray.push(new Item(soupsArray, meatsArray));
        }
        return itemsArray;
    };
    return Item;
}());
exports.Item = Item;
//# sourceMappingURL=menu.js.map