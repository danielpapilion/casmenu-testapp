"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var OrderItemViewModel = (function () {
    function OrderItemViewModel(isNotEmpty, soup, meat) {
        this.isNotEmpty = isNotEmpty;
        this.soup = soup;
        this.meat = meat;
    }
    OrderItemViewModel.fromJson = function (json) {
        var orderItems = new Array();
        for (var _i = 0, _a = json.orderItems; _i < _a.length; _i++) {
            var orderItem = _a[_i];
            orderItems.push(new OrderItemViewModel(!orderItem.isEmpty, orderItem.soup, orderItem.meat));
        }
        return orderItems;
    };
    OrderItemViewModel.toOrderItem = function (viewModel) {
        return new OrderItem(!viewModel.isNotEmpty, viewModel.soup, viewModel.meat);
    };
    OrderItemViewModel.equals = function (one, other) {
        if (one.length != other.length)
            return false;
        else {
            for (var i = 0; i < one.length; i++) {
                if (one[i].isNotEmpty !== other[i].isNotEmpty ||
                    one[i].soup !== other[i].soup || one[i].meat !== other[i].meat)
                    return false;
            }
        }
        return true;
    };
    return OrderItemViewModel;
}());
exports.OrderItemViewModel = OrderItemViewModel;
var OrderItem = (function () {
    function OrderItem(isEmpty, soup, meat) {
        this.isEmpty = isEmpty;
        this.soup = soup;
        this.meat = meat;
    }
    return OrderItem;
}());
exports.OrderItem = OrderItem;
var User = (function () {
    function User(name, email) {
        this.name = name;
        this.email = email;
    }
    User.fromJson = function (json) {
        return new User(json.user.name, json.user.email);
    };
    return User;
}());
exports.User = User;
var Order = (function () {
    function Order(user, orderItems, prise, payed) {
        this.user = user;
        this.orderItems = orderItems;
        this.prise = prise;
        this.payed = payed;
    }
    return Order;
}());
exports.Order = Order;
//# sourceMappingURL=order.js.map