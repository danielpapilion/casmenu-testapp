﻿export class Soup {
    constructor(public name: string) { }

}

export class Meat {
    constructor(public name: string) { }

}

export class Item {
    constructor(
        public soups: Array<Soup>,
        public meats: Array<Meat>) { }

    public static fromJson(json: any): Item[] {
        var itemsArray = new Array<Item>();
        for (var item of json.items) {

            var soupsArray = new Array<Soup>();
            for (var soup of item.soups) {
                soupsArray.push(new Soup(soup.name));
            }

            var meatsArray = new Array<Meat>();
            for (var meat of item.meats) {
                meatsArray.push(new Meat(meat.name));
            }

            itemsArray.push(new Item(soupsArray, meatsArray));
        }

        return itemsArray;
    }

}