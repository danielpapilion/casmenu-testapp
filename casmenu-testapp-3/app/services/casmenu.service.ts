﻿import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';

@Injectable()
export class CasMenuService {
    constructor(private _http: Http) { }

    get(url: string, week: number = 0): Observable<any> {
        let params = new URLSearchParams();
        params.set('week', week.toString());
        let options = new RequestOptions();
        options.search = params;

        return this._http.get(url, { search: params })
        .map((response: Response) => <any>response.json())
        .catch(this.handleError);
    }

    post(url: string, week: number, model: any): Observable<any> {
        let body = JSON.stringify(model);
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let params = new URLSearchParams();
        params.set('week', week.toString());
        let options = new RequestOptions({ headers: headers, search: params });

        return this._http.post(url, body, options)
            .map((response: Response) => <any>response.json())
            .catch(this.handleError);
    }

    delete(url: string, id: number): Observable<any> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this._http.delete(url + id, options)
            .map((response: Response) => <any>response.json())
            .catch(this.handleError);
    }

    private handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.json().error || 'Server error');
    }
}