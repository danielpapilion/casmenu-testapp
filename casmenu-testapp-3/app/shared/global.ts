﻿export class Global {
    public static BASE_MENU_ENDPOINT = 'api/menu/';
    public static BASE_ORDER_ENDPOINT = 'api/order/';
    public static BASE_USER_ENDPOINT = 'api/userinfo';
}