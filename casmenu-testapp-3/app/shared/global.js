"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Global = (function () {
    function Global() {
    }
    return Global;
}());
Global.BASE_MENU_ENDPOINT = 'api/menu/';
Global.BASE_ORDER_ENDPOINT = 'api/order/';
Global.BASE_USER_ENDPOINT = 'api/userinfo';
exports.Global = Global;
//# sourceMappingURL=global.js.map