﻿export interface IDictionary<T> {
    [index: number]: T;
}