"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var Casmenuservice = require("./services/casmenu.service");
var CasMenuService = Casmenuservice.CasMenuService;
var Order = require("./models/order");
var User = Order.User;
var Global = require("./shared/global");
var Global1 = Global.Global;
var router_1 = require("@angular/router");
var platform_browser_1 = require("@angular/platform-browser");
var AppComponent = (function () {
    function AppComponent(router, title, menuService) {
        var _this = this;
        this.menuService = menuService;
        this.indLoading = false;
        router.events.subscribe(function (event) {
            title.setTitle("CasMenu - " + _this.getTitleFor(router.url));
            _this.active = router.url;
        });
    }
    AppComponent.prototype.ngOnInit = function () { this.loadUser(); };
    AppComponent.prototype.loadUser = function () {
        var _this = this;
        this.indLoading = true;
        this.menuService.get(Global1.BASE_USER_ENDPOINT)
            .subscribe(function (result) {
            _this.user = new User(result.name, result.email);
        }, function (error) { return _this.msg = error; });
        this.indLoading = false;
    };
    AppComponent.prototype.getTitleFor = function (url) {
        if (url == "/menu") {
            return "Menü";
        }
        if (url == "/ordermenu") {
            return "Rendelés";
        }
    };
    return AppComponent;
}());
AppComponent = __decorate([
    core_1.Component({
        selector: "casmenu-app",
        templateUrl: "app/app.component.html",
        styleUrls: ["app/style.css"]
    }),
    __metadata("design:paramtypes", [router_1.Router, platform_browser_1.Title, CasMenuService])
], AppComponent);
exports.AppComponent = AppComponent;
//# sourceMappingURL=app.component.js.map