"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var router_1 = require("@angular/router");
var menu_component_1 = require("./components/menu.component");
var ordermenu_component_1 = require("./components/ordermenu.component");
var appRoutes = [
    { path: '', redirectTo: 'ordermenu', pathMatch: 'full' },
    { path: 'menu', component: menu_component_1.MenuComponent },
    { path: 'ordermenu', component: ordermenu_component_1.OrderMenuComponent }
];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map