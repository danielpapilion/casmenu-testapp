﻿using System;
using System.Collections.Generic;
using System.Linq;
using casmenu_testapp_3.Extensions;
using casmenu_testapp_3.Interfaces;
using casmenu_testapp_3.Models;

namespace casmenu_testapp_3.Services
{
    public class SheetsDataRepository : IDataRepository
    {
        private readonly ISheetsApiService _sheetsApiService;

        public SheetsDataRepository(ISheetsApiService sheetsApiService)
        {
            _sheetsApiService = sheetsApiService;
        }


        public Menu GetMenu(int week)
        {
            var initData = GetSheetData(week).FindData<Menu>();

            return new Menu(initData);
        }

        public Menu UpdateMenu(int week, Menu menu)
        {
            var data = new List<List<string>> { menu.ToSheetDataFormat() };
            var sheetTitle = CreateSheetTitle(week);

            var updatedData = Update<Menu>(sheetTitle, SheetConstants.MenuRowIndex + 1, data).FormatToMenuData();
            return new Menu(updatedData);
        }

        public Order GetOrder(int week, string email, string name)
        {
            var data = GetSheetData(week);
            var initData = data.FindData<Order>(email, name);

            if (initData == null)
            {
                return null;
            }

            return new Order(initData);
        }

        public Order UpdateOrder(int week, Order order)
        {
            var data = new List<List<string>> { order.ToSheetDataFormat() };

            var sheetData = GetSheetData(week);
            var rowIndex = sheetData
                               .FindIndex(t => t == sheetData.Filter(order.User.Email, order.User.Name).FirstOrDefault()) + 1;

            var updatedData = Update<Order>(CreateSheetTitle(week), rowIndex, data);
            return new Order(updatedData);
        }

        public Order CreateEmptyOrder(int week, string email, string name)
        {
            var order = new Order
            {
                User = new User(name: name, email: email),
                OrderItems = new List<OrderItem>
                {
                    new OrderItem(),
                    new OrderItem(),
                    new OrderItem(),
                    new OrderItem(),
                    new OrderItem()
                }
            };

            CreateOrder(week, order);
            return order;
        }

        private void CreateOrder(int week, Order order)
        {
            var sheetTitle = CreateSheetTitle(week);
            var data = new List<List<string>> { order.ToSheetDataFormat() };

            _sheetsApiService.Append(sheetTitle, data);
        }

        public bool WeekDataExist(int week)
        {
            var sheetTitles = _sheetsApiService.GetSheetTitles();
            return sheetTitles.Contains(CreateSheetTitle(week));
        }

        private List<string> Update<T>(string sheetTitle, int rowIndex, List<List<string>> data)
        {
            var updatedRow = _sheetsApiService.Update(sheetTitle, rowIndex, rowIndex, data).FirstOrDefault();

            if (updatedRow == null)
                throw new Exception(typeof(T) + " has not been updated.");

            var updatedRowData = updatedRow.Select(x => (string)x).ToList();
            return updatedRowData;
        }

        private List<List<string>> GetSheetData(int week)
        {
            var sheet = _sheetsApiService.GetSheet(CreateSheetTitle(week));
            var gridData = sheet?.Data.FirstOrDefault();

            return gridData?.RowDataAsString();
        }

        private static string CreateSheetTitle(int week)
        {
            return week + ".hét";
        }

    }
}