﻿using System;
using System.Collections.Generic;
using System.Linq;
using casmenu_testapp_3.Interfaces;
using Google.Apis.Sheets.v4;
using Google.Apis.Sheets.v4.Data;


namespace casmenu_testapp_3.Services
{
    public class SheetsApiService : ISheetsApiService
    {
        private readonly IGoogleService _googleService;
        private readonly string _spreadsheetId;

        public SheetsApiService(string spreadsheetId, IGoogleService googleService)
        {
            _googleService = googleService;
            _spreadsheetId = spreadsheetId;
        }

        public Sheet GetSheet(string sheetTitle)
        {
            var request = _googleService.GetSheetsService().Spreadsheets.Get(_spreadsheetId);
            request.IncludeGridData = true;
            request.Ranges = sheetTitle;

            var result = request.Execute();
            return result.Sheets.FirstOrDefault();
        }

        public List<string> GetSheetTitles()
        {
            var request = _googleService.GetSheetsService().Spreadsheets.Get(_spreadsheetId);

            var result = request.Execute();
            return (from sheet in result.Sheets where sheet.Properties.SheetId != null select sheet.Properties.Title).ToList();
        }

        private Dictionary<string, int> GetSheetIDsByName()
        {
            var sheetIdsByName = new Dictionary<string, int>();
            var request = _googleService.GetSheetsService().Spreadsheets.Get(_spreadsheetId);

            var result = request.Execute();
            foreach (var sheet in result.Sheets)
            {
                if (sheet.Properties.SheetId != null)
                    sheetIdsByName.Add(sheet.Properties.Title, (int)sheet.Properties.SheetId);
            }

            return sheetIdsByName;
        }

        public void Append(string sheetTitle, List<List<string>> values)
        {
            var requestBody = new ValueRange { Values = new List<IList<object>>() };
            foreach (var value in values)
            {
                var strValues = value.Select(x => (object)x).ToList();
                requestBody.Values.Add(strValues);
            }

            var request = _googleService.GetSheetsService().Spreadsheets.Values
                .Append(requestBody, _spreadsheetId, sheetTitle);
            request.ValueInputOption = SpreadsheetsResource.ValuesResource.AppendRequest.ValueInputOptionEnum.USERENTERED;

            request.Execute();
        }

        public IList<IList<object>> Update(string sheetTitle, int fromRow, int toRow, List<List<string>> values)
        {
            if (values == null || fromRow < 0 || toRow < 0 || fromRow > toRow) return new List<IList<object>>();

            var range = sheetTitle;
            if (fromRow != 0 && toRow != 0)
                range += "!" + fromRow + ":" + toRow;

            var requestBody = new ValueRange { Values = new List<IList<object>>() };
            foreach (var value in values)
            {
                var strValues = value.Select(x => (object)x).ToList();
                requestBody.Values.Add(strValues);
            }

            var request = _googleService.GetSheetsService().Spreadsheets.Values
                .Update(requestBody, _spreadsheetId, range);
            request.ValueInputOption = SpreadsheetsResource.ValuesResource.UpdateRequest.ValueInputOptionEnum.USERENTERED;
            request.IncludeValuesInResponse = true;

            var response = request.Execute();
            return response.UpdatedRows > 0 ? response.UpdatedData.Values : null;
        }
    }

}