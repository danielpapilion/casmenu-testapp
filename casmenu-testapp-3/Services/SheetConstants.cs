﻿namespace casmenu_testapp_3.Services
{
    public class SheetConstants
    {
        public const int MenuRowIndex = 2;
        public const int OrdersStartRowIndex = 4;

        public const int MenuColumnsCount = 23;
        public const int OrdersColumnsCountWithoutEmail = 23;
        public const int OrdersColumnsCountWithEmail = 24;

        public const int NameColumnIndex = 0;
        public const int PayedColumnIndex = 21;
        public const int PriseColumnIndex = 22;
        public const int EmailColumnIndex = 23;
    }
}