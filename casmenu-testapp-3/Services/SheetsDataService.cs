﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using casmenu_testapp_3.Interfaces;
using casmenu_testapp_3.Models;
using casmenu_testapp_3.Models.Base;

namespace casmenu_testapp_3.Services
{
    public class SheetsDataService : IDataService
    {
        private readonly IDataRepository _sheetsDataRepository;

        public SheetsDataService(IDataRepository sheetsDataRepository)
        {
            _sheetsDataRepository = sheetsDataRepository;
        }

        public Order UpdateOrder(int week, Order order)
        {
            if (_sheetsDataRepository.WeekDataExist(week))
            {
                return _sheetsDataRepository.UpdateOrder(week, order);
            }

            throw InvalidWeekHttpResponseException(week);
        }

        public Menu UpdateMenu(int week, Menu menu)
        {
            if (_sheetsDataRepository.WeekDataExist(week))
            {
                return _sheetsDataRepository.UpdateMenu(week, menu);
            }

            throw InvalidWeekHttpResponseException(week);
        }

        public Order GetOrder(int week, string email, string name)
        {
            if (_sheetsDataRepository.WeekDataExist(week))
            {
                var order = _sheetsDataRepository.GetOrder(week, email, name);
                return order ?? _sheetsDataRepository.CreateEmptyOrder(week, email, name);
            }

            throw InvalidWeekHttpResponseException(week);
        }

        public Menu GetMenu(int week)
        {
            if (_sheetsDataRepository.WeekDataExist(week))
            {
                return _sheetsDataRepository.GetMenu(week);
            }

            throw InvalidWeekHttpResponseException(week);
        }

        private static HttpResponseException InvalidWeekHttpResponseException(int week) =>
            new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound)
            {
                Content = new StringContent($"No data found for week = {week}."),
                ReasonPhrase = "Week not found."
            });
    }

}