﻿using System;
using System.IO;
using System.Threading;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Sheets.v4;
using Google.Apis.Util.Store;
using casmenu_testapp_3.Interfaces;

namespace casmenu_testapp_3.Services.Google
{
    public class GoogleService : IGoogleService
    {
        private readonly string[] _scopes;
        private readonly string _applicationName;
        private readonly string _clientSecretFilePath;

        public GoogleService(string clientSecretFilePath, string applicationName, string[] scopes)
        {
            _clientSecretFilePath = clientSecretFilePath;
            _applicationName = applicationName;
            _scopes = scopes;
        }

        public UserCredential GetGoogleCredential()
        {
            UserCredential credential;

            using (var stream =
                new FileStream(_clientSecretFilePath, FileMode.Open, FileAccess.Read))
            {
                var credPath = System.Environment.GetFolderPath(
                    System.Environment.SpecialFolder.Personal);
                credPath = Path.Combine(credPath, ".credentials/casmenu.json");

                credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    _scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
                Console.WriteLine("Credential file saved to: " + credPath);
            }

            return credential;

        }

        public SheetsService GetSheetsService()
        {
            var credential = GetGoogleCredential();
            var sheetsService = new SheetsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = _applicationName,
            });
            return sheetsService;
        }

    }
}