﻿using System.Web;
using System.Web.Mvc;

namespace casmenu_testapp_3
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
