﻿using System;
using System.Collections.Generic;
using System.Linq;
using casmenu_testapp_3.Models.Base;

namespace casmenu_testapp_3.Models
{
    public class Menu : CasMenuModel
    {
        public List<Item> Items { get; set; }

        public Menu() { }

        public Menu(List<string> data)
        {
            if (data == null || data.Count != 20)
                throw new ArgumentException("Menu data is not valid.");

            Items = new List<Item>();
            for (var i = 0; i < data.Count; i += 4)
            {
                var item = new Item
                {
                    Soups = new List<Food>
                    {
                        new Food(data[i]),
                        new Food(data[i + 1])
                    },
                    Meats = new List<Food>
                    {
                        new Food(data[i + 2]),
                        new Food(data[i + 3])
                    }
                };

                Items.Add(item);
            }
        }

        public override List<string> ToSheetDataFormat()
        {
            var list = new List<string>();

            list.Add("Név");
            foreach (var item in Items)
            {
                list.AddRange(item.Soups.Select(soup => soup.Name));
                list.AddRange(item.Meats.Select(meat => meat.Name));
            }
            list.Add(string.Empty);
            list.Add(string.Empty);

            return list;
        }
    }

}