﻿using System.Collections.Generic;

namespace casmenu_testapp_3.Models
{
    public class Item
    {
        public List<Food> Soups { get; set; }
    
        public List<Food> Meats { get; set; }

    }
}