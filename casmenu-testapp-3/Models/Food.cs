﻿namespace casmenu_testapp_3.Models
{
    public class Food
    {
        public string Name { get; set; }

        public Food(string name)
        {
            Name = name;
        }

    }
}