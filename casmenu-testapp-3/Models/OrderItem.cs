﻿using System;

namespace casmenu_testapp_3.Models
{
    public class OrderItem
    {
        public int Soup { get; set; }
        public int Meat { get; set; }
        public bool IsEmpty { get; set; }

        public OrderItem(int soup, int meat)
        {
            Soup = soup;
            Meat = meat;
            IsEmpty = false;
        }

        public OrderItem()
        {
            IsEmpty = true;
        }


    }
}