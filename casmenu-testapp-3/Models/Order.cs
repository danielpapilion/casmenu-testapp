﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using casmenu_testapp_3.Models.Base;
using casmenu_testapp_3.Services;

namespace casmenu_testapp_3.Models
{
    public class Order : CasMenuModel
    {
        public User User { get; set; }
        public List<OrderItem> OrderItems { get; set; }
        public int Prise { get; set; }
        public bool Payed { get; set; }

        public Order()
        {
            User = new User(name: "", email: "");
            Payed = false;
            Prise = 0;
            OrderItems = new List<OrderItem>();
        }

        public Order(List<string> data)
        {
            if (data == null || data.Count != 23 && data.Count != 24)
                throw new ArgumentException("Order data is not valid.");

            OrderItems = new List<OrderItem>();

            for (int i = 1, week = 0; i < SheetConstants.PayedColumnIndex; i += 4, week++)
            {
                for (int j = i; j < i + 4; j++)
                {
                    data[j] = data[j] == null ? "" : data[j];
                    if (data[j] != "-" && data[j] != "" && data[j] != "x")
                        throw new ArgumentException("Order data is not valid.");
                }

                if (data[i] == "x" || data[i] == "")
                {
                    var soup = (data[i] == "x" && data[i + 1] == "" ? 0 : 1);
                    var meat = (data[i + 2] == "x" && data[i + 3] == "" ? 0 : 1);

                    OrderItems.Add(new OrderItem(soup, meat));
                }
                else if (data[i] == "-")
                {
                    OrderItems.Add(new OrderItem());
                }
            }

            User = new User()
            {
                Name = data[SheetConstants.NameColumnIndex],
                Email = data.Count == SheetConstants.EmailColumnIndex + 1 ? data[SheetConstants.EmailColumnIndex] : null
            };
            Payed = data[SheetConstants.PayedColumnIndex] == "fizetve";
            Prise = int.Parse(data[SheetConstants.PriseColumnIndex], NumberStyles.Number | NumberStyles.Currency);
        }

        public override List<string> ToSheetDataFormat()
        {
            var list = new List<string>();

            list.Add(User.Name);
            foreach (var orderItem in OrderItems)
            {
                if (!orderItem.IsEmpty)
                {
                    var option1 = new List<string> { "x", string.Empty };
                    var option2 = new List<string> { string.Empty, "x" };

                    if (orderItem.Soup == 0)
                        list.AddRange(option1);
                    else if (orderItem.Soup == 1)
                        list.AddRange(option2);

                    if (orderItem.Meat == 0)
                        list.AddRange(option1);
                    else if (orderItem.Meat == 1)
                        list.AddRange(option2);
                }
                else
                {
                    list.AddRange(Enumerable.Repeat<string>("-", 4));
                }
            }

            list.Add(Payed ? "fizetve" : "");
            list.Add(Prise.ToString());
            list.Add(User.Email ?? "");

            return list;
        }

    }
}