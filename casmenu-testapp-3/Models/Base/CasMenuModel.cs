﻿using System.Collections.Generic;

namespace casmenu_testapp_3.Models.Base
{
    public abstract class CasMenuModel
    {
        protected CasMenuModel() { }
        protected CasMenuModel(List<string> data) { }
        public abstract List<string> ToSheetDataFormat();
    }
}