﻿using casmenu_testapp_3.Models;
using casmenu_testapp_3.Models.Base;

namespace casmenu_testapp_3.Interfaces
{
    public interface IDataService
    {
        Order UpdateOrder(int week, Order order);
        Menu UpdateMenu(int week, Menu menu);
        Order GetOrder(int week, string email, string name);
        Menu GetMenu(int week);
    }
}