﻿using casmenu_testapp_3.Models;
using casmenu_testapp_3.Models.Base;

namespace casmenu_testapp_3.Interfaces
{
    public interface IDataRepository
    {
        Menu GetMenu(int week);
        Menu UpdateMenu(int week, Menu menu);
        Order UpdateOrder(int week, Order order);
        Order GetOrder(int week, string email, string name);
        Order CreateEmptyOrder(int week, string email, string name);
        bool WeekDataExist(int week);
    }
}
