﻿using Google.Apis.Auth.OAuth2;
using Google.Apis.Sheets.v4;

namespace casmenu_testapp_3.Interfaces
{
    public interface IGoogleService
    {
        UserCredential GetGoogleCredential();
        SheetsService GetSheetsService();
    }
}