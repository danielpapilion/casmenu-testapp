﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Google.Apis.Sheets.v4.Data;

namespace casmenu_testapp_3.Interfaces
{
    public interface ISheetsApiService
    {
        Sheet GetSheet(string sheetTitle);
        IList<IList<object>> Update(string sheetTitle, int fromRow, int toRow, List<List<string>> values);
        void Append(string sheetTitle, List<List<string>> values);
        List<string> GetSheetTitles();
    }
}