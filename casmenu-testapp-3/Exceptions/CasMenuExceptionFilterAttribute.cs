﻿using System.IO;
using System.Net;
using System.Net.Http;
using System.Web.Http.Filters;

namespace casmenu_testapp_3.Exceptions
{
    public class CasMenuExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(HttpActionExecutedContext context)
        {
            if (context.Exception is IOException)
            {
                context.Response = new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }
    }
}