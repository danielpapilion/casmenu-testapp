﻿using System.Diagnostics;
using System.Web.Http.ExceptionHandling;

namespace casmenu_testapp_3.Exceptions
{
    class TraceExceptionLogger : ExceptionLogger
    {
        public override void Log(ExceptionLoggerContext context)
        {
            Trace.TraceError(context.ExceptionContext.Exception.ToString());
        }
    }
}