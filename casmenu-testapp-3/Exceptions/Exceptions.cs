﻿using System;

namespace casmenu_testapp_3.Exceptions
{
    public class InvalidSpreadsheetException : Exception
    {
        public InvalidSpreadsheetException(string message) : base(message)
        {

        }

        public InvalidSpreadsheetException(string message, Exception inner)
            : base(message, inner)
        {

        }
    }

    public class InvalidCasMenuDataException : Exception
    {
        public InvalidCasMenuDataException(string message) : base(message)
        {

        }

        public InvalidCasMenuDataException(string message, Exception inner)
            : base(message, inner)
        {

        }
    }

}