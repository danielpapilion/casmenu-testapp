﻿using System;
using System.Collections.Generic;
using System.Linq;
using casmenu_testapp_3.Models;
using FluentAssertions;
using Xunit;

namespace casmenu_testapp_3.Test.Models
{
    public class OrderTests
    {
        [Fact]
        public void OrderFromStringListTest()
        {
            var wrongData = new List<string>
            {
                "Gipsz Jakab",
                "x","","","x","","x","","x","x","","","x","x","","x","","-","-","-",
                "fizetve",
                "1200",
                "mail@email.com"
            };
            Action f = () =>
            {
                var order1 = new Order(wrongData);
            };
            f.ShouldThrow<ArgumentException>("Order data is not valid.");


            var rightData = new List<string>
            {
                "Gipsz Jakab",
                "x","","","x","","x","","x","x","","","x","x","","x","","-","-","-","-",
                "fizetve",
                "1200",
                "mail@email.com"
            };
            var order = new Order(rightData);
            order.OrderItems.Should().NotBeNull();
            order.OrderItems.Should().NotBeEmpty();
        }

        [Fact]
        public void OrderToStringListTest()
        {
            var userWithEmail = new User
            {
                Email = "mail@email.com",
                Name = "Gipsz Jakab"
            };
            var userWithNoEmail = new User
            {
                Name = "Gipsz Jakab"
            };

            var listWithEmail = new List<string>
            {
                "Gipsz Jakab",
                "x","","","x","","x","","x","x","","","x","x","","x","","-","-","-","-",
                "fizetve",
                "1200",
                "mail@email.com"
            };

            var listWithNoEmail = new List<string>
            {
                "Gipsz Jakab",
                "x","","","x","","x","","x","x","","","x","x","","x","","-","-","-","-",
                "fizetve",
                "1200",
                ""
            };

            var order = new Order
            {
                User = userWithEmail,
                OrderItems = new List<OrderItem>
                {
                    new OrderItem(0, 1),
                    new OrderItem(1, 1),
                    new OrderItem(0, 1),
                    new OrderItem(0, 0),
                    new OrderItem()
                },
                Payed = true,
                Prise = 1200
            };
            var result1 = order.ToSheetDataFormat();
            result1.ShouldBeEquivalentTo(listWithEmail);


            order.User = userWithNoEmail;
            var result2 = order.ToSheetDataFormat();
            result2.ShouldBeEquivalentTo(listWithNoEmail);
        }

    }
}
