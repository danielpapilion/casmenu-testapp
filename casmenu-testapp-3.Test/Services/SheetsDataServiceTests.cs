﻿using System.Collections.Generic;
using casmenu_testapp_3.Interfaces;
using casmenu_testapp_3.Models;
using Google.Apis.Sheets.v4.Data;
using Moq;
using Xunit;
using casmenu_testapp_3.Services;
using FluentAssertions;

namespace casmenu_testapp_3.Test.Services
{
    public class SheetsDataServiceTests
    {
        [Fact]
        public void GetMenuTest()
        {
            //29. hét
            var sheet = new Sheet
            {
                Properties = new SheetProperties {Title = "29.hét"},
                Data = new List<GridData>
                {
                    new GridData
                    {
                        RowData = new List<RowData>
                        {
                            new RowData
                            {
                                Values = new List<CellData>
                                {
                                    new CellData
                                    {
                                        FormattedValue = "Header row 1"
                                    },
                                }
                            },
                            new RowData
                            {
                                Values = new List<CellData>
                                {
                                    new CellData
                                    {
                                        FormattedValue = "Header row 2"
                                    },
                                }
                            },
                            new RowData
                            {
                                Values = new List<CellData>
                                {
                                    new CellData(),
                                    new CellData {FormattedValue = "Csontleves"},
                                    new CellData {FormattedValue = "Tejfölös zöldbab- leves"},
                                    new CellData {FormattedValue = "Rizseshús"},
                                    new CellData {FormattedValue = "Hentes- tokány, orsó tészta"},
                                    new CellData {FormattedValue = "Csontleves"},
                                    new CellData {FormattedValue = "Burgonya krémleves"},
                                    new CellData {FormattedValue = "Aranygaluska"},
                                    new CellData {FormattedValue = "Roston csirkemell, sült zöldségek, röszti"},
                                    new CellData {FormattedValue = "Csontleves" },
                                    new CellData {FormattedValue = "Tojásleves" },
                                    new CellData {FormattedValue = "Rántott csirkeszárny, burgonya- püré"},
                                    new CellData {FormattedValue = "Natúr szelet, sajtmártás, burgonya- püré"},

                                    new CellData {FormattedValue = "Csontleves"},
                                    new CellData {FormattedValue = "Gyümölcs- leves"},
                                    new CellData {FormattedValue = "Rántott halfilé, mexikói köret, petrezselymes burgonya"},
                                    new CellData {FormattedValue = "Csirke- pörkölt, tarhonya"},
                                    new CellData {FormattedValue = "Csontleves"},
                                    new CellData {FormattedValue = "Májgaluska leves"},
                                    new CellData {FormattedValue = "Sertés apró vadas- mártásban, főtt tészta"},
                                    new CellData {FormattedValue = "Szatymazi csirkemell, sült burgonya"},
                                    new CellData(),
                                    new CellData()
                                }
                            },
                        }
                    }
                }
            };

            var sheetsApiServiceMock = new Mock<ISheetsApiService>();
            sheetsApiServiceMock.Setup(x => x.GetSheet("29.hét")).Returns(sheet);

            //29.hét menü
            var menu = new Menu
            {
                Items = new List<Item>()
                {
                    new Item
                    {
                        Soups = new List<Food> {new Food("Csontleves"), new Food("Tejfölös zöldbab- leves")},
                        Meats = new List<Food> {new Food("Rizseshús"), new Food("Hentes- tokány, orsó tészta")}
                    },
                    new Item
                    {
                        Soups = new List<Food> {new Food("Csontleves"), new Food("Burgonya krémleves")},
                        Meats = new List<Food> {new Food("Aranygaluska"), new Food("Roston csirkemell, sült zöldségek, röszti")}
                    },
                    new Item
                    {
                        Soups = new List<Food> {new Food("Csontleves"), new Food("Tojásleves") },
                        Meats = new List<Food> {new Food("Rántott csirkeszárny, burgonya- püré"), new Food("Natúr szelet, sajtmártás, burgonya- püré") }
                    },                    new Item
                    {
                        Soups = new List<Food> {new Food("Csontleves"), new Food("Gyümölcs- leves")},
                        Meats = new List<Food> {new Food("Rántott halfilé, mexikói köret, petrezselymes burgonya"), new Food("Csirke- pörkölt, tarhonya") }
                    },                    new Item
                    {
                        Soups = new List<Food> {new Food("Csontleves"), new Food("Májgaluska leves") },
                        Meats = new List<Food> {new Food("Sertés apró vadas- mártásban, főtt tészta"), new Food("Szatymazi csirkemell, sült burgonya") }
                    }
                }
            };

            //TODO
            //var sheetsDataService = new SheetsDataService(sheetsApiServiceMock.Object);

            //sheetsDataService.Get<Menu>(29).ShouldBeEquivalentTo(menu);
            //sheetsApiServiceMock.Verify(x => x.GetSheet("29.hét"), Times.Once);
        }

        [Fact]
        public void GetOrderTest()
        {
            //TODO
        }

        [Fact]
        public void OrderTest()
        {
            //TODO
        }

    }
}
