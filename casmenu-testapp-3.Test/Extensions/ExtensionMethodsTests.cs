﻿using System;
using System.Collections.Generic;
using System.Linq;
using casmenu_testapp_3.Exceptions;
using casmenu_testapp_3.Extensions;
using casmenu_testapp_3.Models;
using FluentAssertions;
using Google.Apis.Sheets.v4.Data;
using Xunit;

namespace casmenu_testapp_3.Test.Extensions
{

    public class ExtensionMethodsTests
    {
        #region constants

        private const int MenuColumnsCount = 23;
        private const int OrdersColumnsCountWithoutEmail = 23;
        private const int OrdersColumnsCountWithEmail = 24;
        
        #endregion

        #region IsValidCasMenu

        [Fact]
        public void IsValidCasMenuDataWithEmptyDataTest()
        {
            var data = new List<List<string>>();

            data.IsValidCasMenuData().Should().BeFalse();
        }

        [Fact]
        public void IsValidCasMenuDataWithNotEnoughRowsTest()
        {
            var data = new List<List<string>>();
            var row = Enumerable.Repeat("data", 20).ToList();
            data.Add(row);
            data.Add(row);

            data.IsValidCasMenuData().Should().BeFalse();
        }

        [Fact]
        public void IsValidCasMenuDataMenuDataTest()
        {
            var data = new List<List<string>>();
            var headerRow = Enumerable.Repeat("data", 22).ToList();
            data.Add(headerRow);
            data.Add(headerRow);

            var dataWithInvalidMenuData = new List<List<string>>(data);
            var invalidMenuRow = Enumerable.Repeat("data", 20).ToList();
            dataWithInvalidMenuData.Add(invalidMenuRow);
            dataWithInvalidMenuData.IsValidCasMenuData().Should().BeFalse();

            var dataWithValidMenuData = new List<List<string>>(data);
            var validMenuRow = Enumerable.Repeat("data", MenuColumnsCount).ToList();
            dataWithValidMenuData.Add(validMenuRow);
            dataWithValidMenuData.IsValidCasMenuData().Should().BeTrue();
        }

        [Fact]
        public void IsValidCasMenuDataWithNoOrderDataTest()
        {
            var data = new List<List<string>>();
            var headerRow = Enumerable.Repeat("data", 23).ToList();
            data.Add(headerRow);
            data.Add(headerRow);
            var menuRow = Enumerable.Repeat("data", MenuColumnsCount).ToList();
            data.Add(menuRow);

            data.IsValidCasMenuData().Should().BeTrue();
        }

        [Fact]
        public void IsValidCasMenuDataOrderDataTest()
        {
            var data = new List<List<string>>();
            var headerRow = Enumerable.Repeat("data", 23).ToList();
            data.Add(headerRow);
            data.Add(headerRow);
            var menuRow = Enumerable.Repeat("data", MenuColumnsCount).ToList();
            data.Add(menuRow);
            data.Add(headerRow);

            var dataWithInvalidOrderData = new List<List<string>>(data);
            var invalidOrderRow = Enumerable.Repeat("data", 22).ToList();

            dataWithInvalidOrderData.Add(invalidOrderRow);
            dataWithInvalidOrderData.IsValidCasMenuData().Should().BeFalse();

            var dataWithValidOrderData = new List<List<string>>(data);
            var validOrderRowWithoutEmail = Enumerable.Repeat("data", OrdersColumnsCountWithoutEmail).ToList();
            var validOrderRowWithEmail = Enumerable.Repeat("data", OrdersColumnsCountWithEmail).ToList();
            dataWithValidOrderData.Add(validOrderRowWithEmail);
            dataWithValidOrderData.Add(validOrderRowWithoutEmail);

            dataWithValidOrderData.IsValidCasMenuData().Should().BeTrue();
        }

        #endregion

        #region FindData<Menu>

        [Fact]
        public void FindMenuDataWithInvalidDataTest()
        {
            var data = new List<List<string>>();
            var row = Enumerable.Repeat("data", 20).ToList();
            data.Add(row);
            data.Add(row);

            Action f = () => data.FindData<Menu>();
            f.ShouldThrow<InvalidCasMenuDataException>();         
        }

        [Fact]
        public void FindMenuDataWithValidDataTest()
        {
            var data = new List<List<string>>();
            var headerRow = Enumerable.Repeat("data", 22).ToList();
            data.Add(headerRow);
            data.Add(headerRow);
            var validMenuRow = Enumerable.Repeat("data", MenuColumnsCount).ToList();
            data.Add(validMenuRow);
            data.Add(Enumerable.Repeat("data", OrdersColumnsCountWithEmail).ToList());
            data.Add(Enumerable.Repeat("data", OrdersColumnsCountWithoutEmail).ToList());

            var menuData = new List<string>(Enumerable.Repeat("data", 20).ToList());

            menuData.ShouldBeEquivalentTo(data.FindData<Menu>());
        }

        #endregion

        #region FindData<UpdateOrder>

        public void FindOrderTest()
        {
            //equals to Filter test.
        }

        #endregion

        #region Filter

        [Fact]
        public void FilterWithOneMatchTest()
        {
            var data = new List<List<string>>();
            var list = new List<string>
            {
                "data",
                "data",
                "Kovács János",
                "data",
                "kovijani@email.com"
            };
            data.Add(Enumerable.Repeat("data", 5).ToList());
            data.Add(list);

            var result = new List<List<string>>
            {
                list
            };
            data.Filter("Kovács János", "kovijani@email.com").ShouldBeEquivalentTo(result);
        }

        [Fact]
        public void FilterWithNoMatchTest()
        {
            var data = new List<List<string>>();
            var list = new List<string>
            {
                "data",
                "data",
                "Kovács János",
                "data",
                "data"
            };
            data.Add(Enumerable.Repeat("data", 5).ToList());
            data.Add(list);

            var result = new List<List<string>>();
            data.Filter("Juhász János").ShouldBeEquivalentTo(result);
        }

        [Fact]
        public void FilterWithMoreMatchTest()
        {
            var data = new List<List<string>>();
            var list1 = new List<string>
            {
                "adat",
                "data",
                "Kovács János",
                "data",
                "data"
            };
            data.Add(Enumerable.Repeat("data", 5).ToList());
            data.Add(list1);
            var list2 = new List<string>
            {
                "data",
                "data",
                "Kovács János",
                "data",
                "adat"
            };
            data.Add(list2);
            data.Add(Enumerable.Repeat("data", 5).ToList());

            var result = new List<List<string>>
            {
                list1, list2
            };
            data.Filter("Kovács János", "adat").ShouldBeEquivalentTo(result);
        }

        #endregion

        #region SheetToList

        [Fact]
        public void SheetToListTest()
        {
            var gridData = new GridData { RowData = new List<RowData>() };
            for (var i = 0; i < 3; i++)
            {
                var rowData = new RowData { Values = new List<CellData>() };
                for (var j = 0; j < 3; j++)
                {
                    var cellData = new CellData { FormattedValue = "data[" + i + "][" + j + "]" };
                    rowData.Values.Add(cellData);
                }
                gridData.RowData.Add(rowData);
            }

            var data = new List<GridData> { gridData };
            var sheet = new Sheet { Data = data };

            var result = new List<List<string>>();
            for (var i = 0; i < 3; i++)
            {
                var row = new List<string>();
                for (var j = 0; j < 3; j++)
                {
                    row.Add("data[" + i + "][" + j + "]");
                }
                result.Add(row);
            }

            //sheet.ToList().ShouldBeEquivalentTo(result);
        }

        [Fact]
        public void EmptySheetToListTest()
        {
            var gridData = new GridData { RowData = new List<RowData>() };
            var data = new List<GridData> { gridData };
            var sheet = new Sheet { Data = data };

            var result = new List<List<string>>();

            //sheet.ToList().ShouldBeEquivalentTo(result);
        }

        #endregion
    }
}
